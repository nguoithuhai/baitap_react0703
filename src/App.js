import logo from "./logo.svg";
import "./App.css";
import Header from "./BaiTapLayoutComponent/Header";
import Body from "./BaiTapLayoutComponent/Body";
import Footer from "./BaiTapLayoutComponent/Footer";
// reupdate
function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      {/* <Banner />
      <Item /> */}
      <Footer />
    </div>
  );
}

export default App;
