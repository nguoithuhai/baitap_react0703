import React, { Component } from "react";
import Banner from "./Banner";
import Item from "./Item";

export default class Body extends Component {
  render() {
    return (
      <div class="border  border-danger">
        {/* // banner */}
        <div class="py-5">
          <div class="container px-lg-5 border  border-primary">
            <Banner />
          </div>
        </div>
        {/* <section className="pt-4"> */}
        {/* item */}
        <div className="container row gx-lg-5 mx-auto ">
          <div className="container col-lg-3 col-xxl-4 mb-5 ">
            <Item />
          </div>
          <div className="col-lg-3 col-xxl-4 mb-5">
            <Item />
          </div>
          <div className="col-lg-3 col-xxl-4 mb-5">
            <Item />
          </div>
          <div className="col-lg-3 col-xxl-4 mb-5">
            <Item />
          </div>
          <div className="col-lg-3 col-xxl-4 mb-5">
            <Item />
          </div>
          <div className="col-lg-3 col-xxl-4 mb-5">
            <Item />
          </div>
          <div className="col-lg-3 col-xxl-4 mb-5">
            <Item />
          </div>
          <div className="col-lg-3 col-xxl-4 mb-5">
            <Item />
          </div>
        </div>

        {/* </section> */}
      </div>
    );
  }
}
